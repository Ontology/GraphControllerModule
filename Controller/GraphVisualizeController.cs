﻿using GraphControllerModule.Factories;
using GraphControllerModule.Models;
using GraphControllerModule.Services;
using GraphControllerModule.Translations;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.StateMachines;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace GraphControllerModule.Controller
{
    public class GraphVisualizeController : GraphVisualizeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ElasticSearch_ServiceAgent serviceAgentElastic;

        private GraphItemFactory graphItemFactory;

        private JsonFactory jsonFactory;

        private clsOntologyItem referenceItem;

        private clsLocalConfig localConfig;

        private bool nodesPrepared = false;
        private bool edgesPrepared = false;
        private bool receivedSelected = false;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public GraphVisualizeController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();

            
        }

        private void GraphVisualizeController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            if (!IsSuccessful_Login) return;
            webSocketServiceAgent.SendPropertyChange(e.PropertyName);


            
        }

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ElasticSearch_ServiceAgent(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            graphItemFactory = new GraphItemFactory(localConfig);
            graphItemFactory.PropertyChanged += GraphItemFactory_PropertyChanged;
            jsonFactory = new JsonFactory();

            
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentElastic = null;
            translationController = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
           

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.SelectedClassNode,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


           
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            referenceItem = oItemSelected;
            receivedSelected = false;
            if (referenceItem != null && referenceItem.Type == localConfig.Globals.Type_Class)
            {
                edgesPrepared = false;
                nodesPrepared = false;
                var result = serviceAgentElastic.GetClassRelations(referenceItem);
                Text_View = "Class: " + referenceItem.Name;
            }
            else if (referenceItem.Type == localConfig.Globals.Type_Object)
            {
                edgesPrepared = false;
                nodesPrepared = false;
                var result = serviceAgentElastic.GetObjectRelations(referenceItem);
                var classItem = serviceAgentElastic.GetOItem(referenceItem.GUID_Parent, localConfig.Globals.Type_Class);

                Text_View = referenceItem.Name + " (" + classItem.Name + ")";
            }

        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ControllerStateMachine.LoginSuccessful))
            {
                Text_View = translationController.Title_GraphView;
                Label_Class = translationController.Label_Classes;
                Label_Object = translationController.Label_Objects;
                webSocketServiceAgent.SetEnable(true);
                IsChecked_Class = true;
                IsChecked_Object = true;

                webSocketServiceAgent.SendModel();

                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        private void GraphItemFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.GraphItemFactory_ResultGraphItems)
            {
                if (graphItemFactory.ResultGraphItems.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var urlList = new List<string>();
                    var nodesFile = Guid.NewGuid().ToString() + ".json";

                    var nameSpaceNode = typeof(clsLocalConfig).Namespace;
                    var nameSpaceEdge = typeof(clsLocalConfig).Namespace;
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(nodesFile);

                    using (sessionFile.StreamWriter)
                    {
                        
                        using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                        {
                            jsonTextWriter.WriteStartArray();
                            foreach(var nodeItem in graphItemFactory.NodeItems)
                            {
                                var result = jsonFactory.WriteItemToJson(nodeItem, jsonTextWriter, nameSpaceNode);
                            }
                            
                            jsonTextWriter.WriteEndArray();
                        }
                    }

                    urlList.Add(sessionFile.FileUri.AbsoluteUri);

                    var edgesFile = Guid.NewGuid().ToString() + ".json";

                    sessionFile = webSocketServiceAgent.RequestWriteStream(edgesFile);

                    using (sessionFile.StreamWriter)
                    {
                        using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                        {
                            jsonTextWriter.WriteStartArray();
                            foreach (var nodeItem in graphItemFactory.EdgeItems)
                            {
                                var result = jsonFactory.WriteItemToJson(nodeItem, jsonTextWriter, nameSpaceEdge);
                            }

                            jsonTextWriter.WriteEndArray();
                        }
                    }

                    urlList.Add(sessionFile.FileUri.AbsoluteUri);

                    UrlList_Nodes = urlList;
                    
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ElasticServiceAgent_ResultClassRelations)
            {
                if (serviceAgentElastic.ResultClassRelations.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var classAttributes = serviceAgentElastic.ClassAttributes;
                    var classRelations = serviceAgentElastic.ClassRelations;

                    var result = graphItemFactory.CreateGraphItems(referenceItem, classAttributes, classRelations); 
                }
            }
            else if (e.PropertyName == Notifications.NotifyChanges.ElasticServiceAgent_ResultObjectRelations)
            {
                if (serviceAgentElastic.ResultObjectRelations.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var objectClasses = serviceAgentElastic.ObjectClasses;
                    var objectAttributes = serviceAgentElastic.ObjectAttributes;
                    var objectRelations = serviceAgentElastic.ObjectRelations;

                    var result = graphItemFactory.CreateGraphItems(referenceItem, objectClasses, objectAttributes, objectRelations);
                }
                
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;

            PropertyChanged += GraphVisualizeController_PropertyChanged;


        }

        

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == "selectedNode")
                {
                    var nodeIdobj = webSocketServiceAgent.Request["NodeId"];
                    if (nodeIdobj != null)
                    {
                        var nodeId = nodeIdobj.ToString();
                        var nodeSplit = nodeId.Split('_');

                        var nodeType = nodeSplit[0];

                        if (nodeType == Notifications.NotifyChanges.NodeType_Class)
                        {
                            var classId = nodeSplit[1];
                            
                            var oItemClass = serviceAgentElastic.GetOItem(classId, localConfig.Globals.Type_Class);

                            if (oItemClass != null)
                            {
                                var interModMessage = new InterServiceMessage
                                {
                                    ChannelId = Channels.SelectClassNode,
                                    OItems = new List<clsOntologyItem> { oItemClass}
                                };
                                webSocketServiceAgent.SendInterModMessage(interModMessage);
                            }
                        }
                        else if (nodeType == Notifications.NotifyChanges.NodeType_ClassAtt)
                        {
                            var classId = nodeSplit[1];
                            var attributeTypeId = nodeSplit[2];


                        }
                        else if (nodeType == Notifications.NotifyChanges.NodeType_Object)
                        {
                            var objectId = nodeSplit[1];

                            var oItemObject = serviceAgentElastic.GetOItem(objectId, localConfig.Globals.Type_Object);

                            if (oItemObject != null)
                            {
                                var interModMessage = new InterServiceMessage
                                {
                                    ChannelId = Channels.ParameterList,
                                    OItems = new List<clsOntologyItem> { oItemObject }
                                };
                                webSocketServiceAgent.SendInterModMessage(interModMessage);
                            }
                        }

                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "selectedEdge")
                {
                    var edgeIdobj = webSocketServiceAgent.Request["EdgeId"];
                    if (edgeIdobj != null)
                    {
                        var edgeId = edgeIdobj.ToString();
                        var edgeSplit = edgeId.Split('_');



                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "NodesPrepared")
                {
                    nodesPrepared = true;
                    CheckPrepare();
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == "EdgesPrepared")
                {
                    edgesPrepared = true;
                    CheckPrepare();
                }


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {
                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectId = webSocketServiceAgent.ObjectArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(objectId)) return;

                var objectItem = serviceAgentElastic.GetOItem(objectId, localConfig.Globals.Type_Object);

                if (objectItem == null) return;

                referenceItem = objectItem;
                receivedSelected = true;
                LocalStateMachine.SetItemSelected(referenceItem);
                
                
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ClassArgument)
            {
                var classId = webSocketServiceAgent.ClassArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(classId)) return;

                var classItem = serviceAgentElastic.GetOItem(classId, localConfig.Globals.Type_Class);

                if (classItem == null) return;

                referenceItem = classItem;
                receivedSelected = true;
                LocalStateMachine.SetItemSelected(referenceItem);
            }


        }

        private void CheckPrepare()
        {
            if (nodesPrepared && edgesPrepared)
            {
                Command_VisGraph = "LoadGraph";
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.SelectedClassNode && IsChecked_Class)
            {
                
                var oItem = message.OItems.FirstOrDefault();

                if (oItem == null) return;
                if (referenceItem != null && oItem.GUID == referenceItem.GUID) return;

                referenceItem = serviceAgentElastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Class);
                receivedSelected = true;
                LocalStateMachine.SetItemSelected(referenceItem);

            }
            else if (message.ChannelId == Channels.ParameterList && IsChecked_Object)
            {
                var oItem = message.OItems.FirstOrDefault();

                if (oItem == null) return;
                if (referenceItem != null && oItem.GUID == referenceItem.GUID) return;

                referenceItem = serviceAgentElastic.GetOItem(oItem.GUID, localConfig.Globals.Type_Object);
                receivedSelected = true;
                LocalStateMachine.SetItemSelected(referenceItem);
                

            }

        }

       
      

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
