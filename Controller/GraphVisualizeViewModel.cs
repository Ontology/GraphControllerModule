﻿using GraphControllerModule.Notifications;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Controller
{
    public class GraphVisualizeViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private string text_View;
        [ViewModel(Send = true)]
        public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private bool isenabled_Graph;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.VisGraph, ViewItemId = "visGraph", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Graph
        {
            get { return isenabled_Graph; }
            set
            {
                if (isenabled_Graph == value) return;

                isenabled_Graph = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Graph);

            }
        }

        private List<string> urllist_Nodes;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.VisGraph, ViewItemId = "visGraph", ViewItemType = ViewItemType.UrlList)]
        public List<string> UrlList_Nodes
        {
            get { return urllist_Nodes; }
            set
            {
                if (urllist_Nodes == value) return;

                urllist_Nodes = value;

                RaisePropertyChanged(NotifyChanges.View_UrlList_Nodes);

            }
        }


        private string url_GraphOptions;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.VisGraph, ViewItemId = "visGraph", ViewItemType = ViewItemType.JsonConfiguration)]
        public string Url_GraphOptions
        {
            get { return url_GraphOptions; }
            set
            {
                if (url_GraphOptions == value) return;

                url_GraphOptions = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Url_GraphOptions);

            }
        }

        private string command_VisGraph;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.VisGraph, ViewItemId = "visGraph", ViewItemType = ViewItemType.Command)]
        public string Command_VisGraph
        {
            get { return command_VisGraph; }
            set
            {

                command_VisGraph = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Command_VisGraph);

            }
        }

        private bool isenabled_Object;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "objectButton", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Object
        {
            get { return isenabled_Object; }
            set
            {
                if (isenabled_Object == value) return;

                isenabled_Object = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Object);

            }
        }

        private string label_Object;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "objectButton", ViewItemType = ViewItemType.Content)]
        public string Label_Object
        {
            get { return label_Object; }
            set
            {
                if (label_Object == value) return;

                label_Object = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Object);

            }
        }

        private bool isenabled_Class;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "classButton", ViewItemType = ViewItemType.Enable)]
        public bool IsEnabled_Class
        {
            get { return isenabled_Class; }
            set
            {
                if (isenabled_Class == value) return;

                isenabled_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Class);

            }
        }

        private string label_Class;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "classButton", ViewItemType = ViewItemType.Content)]
        public string Label_Class
        {
            get { return label_Class; }
            set
            {
                if (label_Class == value) return;

                label_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Class);

            }
        }

        private bool ischecked_Object = true;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "objectButton", ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_Object
        {
            get { return ischecked_Object; }
            set
            {
                if (ischecked_Object == value) return;

                ischecked_Object = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_Object);

            }
        }

        private bool ischecked_Class = true;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.ToggleButton, ViewItemId = "classButton", ViewItemType = ViewItemType.Checked)]
        public bool IsChecked_Class
        {
            get { return ischecked_Class; }
            set
            {

                ischecked_Class = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsChecked_Class);

            }
        }
    }
}
