﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Models
{
    public enum NodeShapeType
    {
        Ellipse = 0,
        Circle = 1,
        Database = 2,
        Box = 3,
        Text = 4,
        Image = 5,
        CircularImage,
        Diamond,
        Dot,
        Star,
        Triangle,
        TriangleDown,
        Square,
        Icon
    }
    public class NodeItem : GraphItem
    {
        [Json(EnumToString = true, EnumStringFirstCharToLowercase = true, PropertyNameToLowercase = true)]
        public NodeShapeType Shape { get; set; }
    }
}
