﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Models
{
    public abstract class GraphItem
    {
        [Json(PropertyNameToLowercase = true)]
        public string Id { get; set; }

        [Json(PropertyNameToLowercase = true)]
        public string Name { get; set; }

        [Json(PropertyNameToLowercase = true)]
        public string Label { get; set; }

        [Json(PropertyNameToLowercase = true)]
        public FontItem Font { get; set; }

        [Json(PropertyNameToLowercase = true)]
        public string Color
        {
            get
            {
                if (ItemColor == null) return null;

                return "#" + ItemColor.Value.ToArgb().ToString("X4").Substring(2);
            }
        }

        public Color? ItemColor { get; set; }


    }
}
