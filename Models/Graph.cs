﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Models
{
    public class Graph
    {
        public List<NodeItem> Nodes { get; set; }
        public List<EdgeItem> Edges { get; set; }
    }
}
