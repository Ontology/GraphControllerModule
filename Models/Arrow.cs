﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Models
{
    public class Arrow
    {
        [Json(PropertyNameToLowercase = true)]
        public bool EnableTo { get; set; }

        [Json(PropertyNameToLowercase = true)]
        public bool EnableMidlle { get; set; }

        [Json(PropertyNameToLowercase = true)]
        public bool EnableFrom { get; set; }
    }
}
