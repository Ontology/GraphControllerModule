﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Models
{
    public enum FontAlign
    {
        left = 0
    }
    public class FontItem
    {
        [Json(Property = "face")]
        public string Face { get; set; }

        [Json(Property = "align", EnumToString = true, PropertyNameToLowercase = true)]
        public FontAlign Align { get; set; }
    }
}
