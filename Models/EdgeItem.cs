﻿using OntoMsg_Module.Attributes;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphControllerModule.Models
{
    public class EdgeItem : GraphItem
    {

        private string from;
        [Json(PropertyNameToLowercase = true)]
        public string From
        {
            get
            {
                return from;
            }
            set
            {
                from = value;
            }
        }

        public string to;
        [Json(PropertyNameToLowercase = true)]
        public string To
        {
            get
            {
                return to;
            }
            set
            {
                to = value;
            }
        }



        private NodeItem leftNode;
        public NodeItem LeftNode
        {
            get
            {
                return leftNode;
            }
            set
            {
                leftNode = value;
                from = leftNode.Id;
            }
        }

        private NodeItem rightNode;
        public NodeItem RightNode
        {
            get
            {
                return rightNode;
            }
            set
            {
                rightNode = value;
                to = rightNode.Id;
            }
        }
        
    }
}
