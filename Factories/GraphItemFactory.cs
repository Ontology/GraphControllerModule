﻿using GraphControllerModule.Models;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GraphControllerModule.Factories
{
    public class GraphItemFactory : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        public List<NodeItem> NodeItems { get; private set; }
        public List<EdgeItem> EdgeItems { get; private set; }

        private clsOntologyItem refClass;
        private List<clsClassRel> classRelations;
        private List<clsClassAtt> classAttributes;

        private clsOntologyItem refObject;
        private List<clsObjectAtt> objectAttributes;
        private List<clsOntologyItem> objectClasses;
        private List<clsObjectRel> objectRelations;

        private Thread createGraphItemsAsync;

        private clsOntologyItem resultGraphItems;
        public clsOntologyItem ResultGraphItems
        {
            get { return resultGraphItems; }
            set
            {
                resultGraphItems = value;
                RaisePropertyChanged(Notifications.NotifyChanges.GraphItemFactory_ResultGraphItems);
            }
        }

        public clsOntologyItem CreateGraphItems(clsOntologyItem refClass, List<clsClassAtt> classAttributes,
            List<clsClassRel> classRelations)
        {
            this.refClass = refClass;
            this.classRelations = classRelations;
            this.classAttributes = classAttributes;

            if (createGraphItemsAsync != null)
            {
                try
                {
                    createGraphItemsAsync.Abort();
                }
                catch(Exception ex)
                {

                }
            }

            createGraphItemsAsync = new Thread(CreateClassesGraphItemsAsync);
            createGraphItemsAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        public clsOntologyItem CreateGraphItems(clsOntologyItem refObject, List<clsOntologyItem> objectClasses,
            List<clsObjectAtt> objectAttributes,
            List<clsObjectRel> objectRelations)
        {
            this.refObject = refObject;
            this.objectClasses = objectClasses;
            this.objectAttributes = objectAttributes;
            this.objectRelations = objectRelations;

            if (createGraphItemsAsync != null)
            {
                try
                {
                    createGraphItemsAsync.Abort();
                }
                catch (Exception ex)
                {

                }
            }

            createGraphItemsAsync = new Thread(CreateObjectRelationsGraphItemsAsync);
            createGraphItemsAsync.Start();

            return localConfig.Globals.LState_Success.Clone();
        }

        private void CreateClassesGraphItemsAsync()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var nodeRelList = classRelations.Select(classRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Class + "_" + classRel.ID_Class_Left,
                Label = classRel.Name_Class_Left,
                Shape = NodeShapeType.Box
            }).ToList();

            nodeRelList.AddRange(classRelations.Select(classRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Class + "_" + classRel.ID_Class_Right,
                Label = classRel.Name_Class_Right,
                Shape = NodeShapeType.Box
            }));

            nodeRelList.AddRange(classAttributes.Select(classAtt => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_ClassAtt + "_" + classAtt.ID_Class + "_" + classAtt.ID_AttributeType,
                Label = classAtt.Name_AttributeType + " (" + classAtt.Name_Class + ")",
                Shape = NodeShapeType.Circle
            }));



            NodeItems = nodeRelList.GroupBy(classItem => new { ID = classItem.Id,
                    Label = classItem.Label,
                    Shape = classItem.Shape }).Select(classItem => new NodeItem { Id = classItem.Key.ID,
                        Label = classItem.Key.Label,
                        Shape = classItem.Key.Shape,
                        Font = new FontItem
                        {
                            Face = "monospace",
                            Align = FontAlign.left
                        },
                        ItemColor = classItem.Key.ID == Notifications.NotifyChanges.NodeType_Class + "_" + refClass.GUID ? System.Drawing.Color.Green : System.Drawing.Color.Yellow
                    }).ToList();

            var edgeRelList = classRelations.GroupBy(classRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Class + "_" + classRel.ID_Class_Left,
                ID_Right = Notifications.NotifyChanges.NodeType_Class + "_" + classRel.ID_Class_Right,
                ID_RelationType = classRel.ID_RelationType,
                Label = classRel.Name_RelationType
            }).Select(grpItem => new
            {
                Id_RelationType = grpItem.Key.ID_RelationType,
                Label = grpItem.Key.Label,
                Id_Left = grpItem.Key.ID_Left,
                Id_Right = grpItem.Key.ID_Right
            }).ToList();


            var edgeItems = new List<EdgeItem>();
            foreach(var edge in edgeRelList)
            {
                var leftNode = NodeItems.FirstOrDefault(nodeItem => nodeItem.Id == edge.Id_Left);
                if (leftNode == null) continue;

                var rightNode = NodeItems.FirstOrDefault(nodeItem => nodeItem.Id == edge.Id_Right);
                if (rightNode == null) continue;

                var edgeItem = new EdgeItem
                {
                    Id = Notifications.NotifyChanges.EdgeType_ClassRel +  edge.Id_Left + "_" + edge.Id_RelationType + "_" + edge.Id_Right,
                    Label = edge.Label,
                    LeftNode = leftNode,
                    RightNode = rightNode,
                    Font = new FontItem
                    {
                        Face = "monospace",
                        Align = FontAlign.left
                    },
                    ItemColor = System.Drawing.Color.Yellow
                };
                edgeItems.Add(edgeItem);
            }

            edgeItems.AddRange(classAttributes.Select(classAtt => new EdgeItem
            {
                Id = Notifications.NotifyChanges.EdgeType_ClassAttRel + classAtt.ID_Class + "_" + classAtt.ID_AttributeType,
                Label = classAtt.Name_AttributeType + " (" + classAtt.Name_Class + ")",
                From = Notifications.NotifyChanges.NodeType_Class + "_" + classAtt.ID_Class,
                To = Notifications.NotifyChanges.NodeType_ClassAtt + "_" + classAtt.ID_Class + "_" + classAtt.ID_AttributeType,
                Font = new FontItem
                {
                    Face = "monospace",
                    Align = FontAlign.left
                },
                ItemColor = System.Drawing.Color.Yellow

            }));
            EdgeItems = edgeItems;

            ResultGraphItems = localConfig.Globals.LState_Success.Clone();
        }

        private void CreateObjectRelationsGraphItemsAsync()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            //// Classes of Object
            //var nodeRelList = objectClasses.Select(objClass => new NodeItem
            //{
            //    Id = Notifications.NotifyChanges.NodeType_Class + "_" + objClass.GUID,
            //    Label = objClass.Name,
            //    Shape = NodeShapeType.Ellipse
            //}).ToList();

            var nodeRelList = objectRelations.Where(objectRel => objectRel.Ontology == localConfig.Globals.Type_Object).Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Object + "_" + objectRel.ID_Object,
                Label = objectRel.Name_Object,
                Shape = NodeShapeType.Box,
                ItemColor = System.Drawing.Color.Yellow
            }).ToList();

            // related Objects
            nodeRelList.AddRange(objectRelations.Where(objectRel => objectRel.Ontology == localConfig.Globals.Type_Object).Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Object + "_" + objectRel.ID_Other,
                Label = objectRel.Name_Other,
                Shape = NodeShapeType.Box,
                ItemColor = System.Drawing.Color.Yellow
            }));

            // related AttributeTypes
            nodeRelList.AddRange(objectRelations.Where(objectRel => objectRel.Ontology == localConfig.Globals.Type_AttributeType).Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_AttType + "_" + objectRel.ID_Other,
                Label = objectRel.Name_Other,
                Shape = NodeShapeType.Diamond,
                ItemColor = System.Drawing.Color.LightBlue
            }));
            
            // related RelationTypes
            nodeRelList.AddRange(objectRelations.Where(objectRel => objectRel.Ontology == localConfig.Globals.Type_RelationType).Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_RelationType + "_" + objectRel.ID_Other,
                Label = objectRel.Name_Other,
                Shape = NodeShapeType.Triangle,
                ItemColor = System.Drawing.Color.Bisque
            }));

            // related Classes
            nodeRelList.AddRange(objectRelations.Where(objectRel => objectRel.Ontology == localConfig.Globals.Type_Class).Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Class + "_" + objectRel.ID_Other,
                Label = objectRel.Name_Other,
                Shape = NodeShapeType.Ellipse,
                ItemColor = System.Drawing.Color.SandyBrown
            }));

            // Classes of related Objects
            nodeRelList.AddRange(objectRelations.Where(objectRel => objectRel.Ontology == localConfig.Globals.Type_Object).Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Class + "_" + objectRel.ID_Parent_Other,
                Label = objectRel.Name_Parent_Other,
                Shape = NodeShapeType.Ellipse,
                ItemColor = System.Drawing.Color.SandyBrown
            }));

            // Classes of left Objects
            nodeRelList.AddRange(objectRelations.Select(objectRel => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_Class + "_" + objectRel.ID_Parent_Object,
                Label = objectRel.Name_Parent_Object,
                Shape = NodeShapeType.Ellipse,
                ItemColor = System.Drawing.Color.SandyBrown
            }));

            // Object-Attributes of Object
            nodeRelList.AddRange(objectAttributes.Select(objAtt => new NodeItem
            {
                Id = Notifications.NotifyChanges.NodeType_ObjectAtt + "_" + objAtt.ID_Attribute,
                Label = objAtt.Val_Name,
                Shape = NodeShapeType.Circle,
                ItemColor = System.Drawing.Color.LightBlue
            }));


            // Create list of nodes
            NodeItems = nodeRelList.GroupBy(nodeRelItem => new {
                ID = nodeRelItem.Id,
                Label = nodeRelItem.Label,
                Shape = nodeRelItem.Shape,
                Color = nodeRelItem.ItemColor
            }).Select(nodeRelItem => new NodeItem
            {
                Id = nodeRelItem.Key.ID,
                Label = nodeRelItem.Key.Label,
                Shape = nodeRelItem.Key.Shape,
                Font = new FontItem
                {
                    Face = "monospace",
                    Align = FontAlign.left
                },
                ItemColor = nodeRelItem.Key.ID == Notifications.NotifyChanges.NodeType_Object + "_" + refObject.GUID ? System.Drawing.Color.Green : nodeRelItem.Key.Color
            }).ToList();

            // Add the object, if it's not in the nodelist
            if (!NodeItems.Any(nodeItem => nodeItem.Id != Notifications.NotifyChanges.NodeType_Object + "_" + refObject.GUID))
            {
                NodeItems.Add(new NodeItem
                {
                    Id = Notifications.NotifyChanges.NodeType_Object + "_" + refObject.GUID,
                    Label = refObject.Name,
                    Shape = NodeShapeType.Box,
                    Font = new FontItem
                    {
                        Face = "monospace",
                        Align = FontAlign.left
                    },
                    ItemColor = System.Drawing.Color.Green
                });
            }

            // Relation between object and other objects
            var edgeRelList = objectRelations.Where(objRel => objRel.Ontology == localConfig.Globals.Type_Object).GroupBy(objRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Object,
                ID_Right = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Other,
                ID_RelationType = objRel.ID_RelationType,
                Label = objRel.Name_RelationType
            }).Select(grpItem => new
            {
                Id_RelationType = grpItem.Key.ID_RelationType,
                Label = grpItem.Key.Label,
                Id_Left = grpItem.Key.ID_Left,
                Id_Right = grpItem.Key.ID_Right
            }).ToList();

            // Relation between object and classes
            edgeRelList.AddRange(objectRelations.Where(objRel => objRel.Ontology == localConfig.Globals.Type_Class).GroupBy(objRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Object,
                ID_Right = Notifications.NotifyChanges.NodeType_Class + "_" + objRel.ID_Other,
                ID_RelationType = objRel.ID_RelationType,
                Label = objRel.Name_RelationType
            }).Select(grpItem => new
            {
                Id_RelationType = grpItem.Key.ID_RelationType,
                Label = grpItem.Key.Label,
                Id_Left = grpItem.Key.ID_Left,
                Id_Right = grpItem.Key.ID_Right
            }));

            // Relation between object and AttributeTypes
            edgeRelList.AddRange(objectRelations.Where(objRel => objRel.Ontology == localConfig.Globals.Type_AttributeType).GroupBy(objRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Object,
                ID_Right = Notifications.NotifyChanges.NodeType_AttType + "_" + objRel.ID_Other,
                ID_RelationType = objRel.ID_RelationType,
                Label = objRel.Name_RelationType
            }).Select(grpItem => new
            {
                Id_RelationType = grpItem.Key.ID_RelationType,
                Label = grpItem.Key.Label,
                Id_Left = grpItem.Key.ID_Left,
                Id_Right = grpItem.Key.ID_Right
            }));

            // Relation between object and RelationTypes
            edgeRelList.AddRange(objectRelations.Where(objRel => objRel.Ontology == localConfig.Globals.Type_RelationType).GroupBy(objRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Object,
                ID_Right = Notifications.NotifyChanges.NodeType_RelationType + "_" + objRel.ID_Other,
                ID_RelationType = objRel.ID_RelationType,
                Label = objRel.Name_RelationType
            }).Select(grpItem => new
            {
                Id_RelationType = grpItem.Key.ID_RelationType,
                Label = grpItem.Key.Label,
                Id_Left = grpItem.Key.ID_Left,
                Id_Right = grpItem.Key.ID_Right
            }));

            // Class of Object
            var edgeRel = objectRelations.Select(objRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Object,
                ID_Right = Notifications.NotifyChanges.NodeType_Class + "_" + objRel.ID_Parent_Object
            }).ToList();

            edgeRel.AddRange(objectRelations.Select(objRel => new
            {
                ID_Left = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Other,
                ID_Right = Notifications.NotifyChanges.NodeType_Class + "_" + objRel.ID_Parent_Other
            }));

            var objClsEdges = edgeRel.GroupBy(edgeRelItem => new { ID_Left = edgeRelItem.ID_Left, ID_Right = edgeRelItem.ID_Right }).Select(objClass => new
            {
                Id_RelationType = "InstanceOf",
                Label = "Instanz von",
                Id_Left = objClass.Key.ID_Left,
                Id_Right = objClass.Key.ID_Right
            });
            edgeRelList.AddRange(objClsEdges);

            // Create the list of EdgeItems
            var edgeItems = new List<EdgeItem>();
            foreach (var edge in edgeRelList)
            {
                var leftNode = NodeItems.FirstOrDefault(nodeItem => nodeItem.Id == edge.Id_Left);
                if (leftNode == null) continue;

                var rightNode = NodeItems.FirstOrDefault(nodeItem => nodeItem.Id == edge.Id_Right);
                if (rightNode == null) continue;

                var edgeItem = new EdgeItem
                {
                    Id = edge.Id_Left + "_" + edge.Id_RelationType + "_" + edge.Id_Right,
                    Label = edge.Label,
                    LeftNode = leftNode,
                    RightNode = rightNode,
                    Font = new FontItem
                    {
                        Face = "monospace",
                        Align = FontAlign.left
                    },
                    ItemColor = System.Drawing.Color.Yellow
                };
                edgeItems.Add(edgeItem);
            }

            // Add the Relation between the ObjectAttributes and the Object
            edgeItems.AddRange(objectAttributes.Select(objRel => new EdgeItem
            {
                Id = Notifications.NotifyChanges.EdgeType_ObjectAtt + objRel.ID_Attribute,
                Label = objRel.Name_AttributeType,
                From = Notifications.NotifyChanges.NodeType_Object + "_" + objRel.ID_Object,
                To = Notifications.NotifyChanges.NodeType_ObjectAtt + "_" + objRel.ID_Attribute,
                Font = new FontItem
                {
                    Face = "monospace",
                    Align = FontAlign.left
                },
                ItemColor = System.Drawing.Color.Yellow

            }));

            EdgeItems = edgeItems;

            ResultGraphItems = localConfig.Globals.LState_Success.Clone();
        }

        public GraphItemFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }
}
