﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GraphControllerModule.Services
{
    public class ElasticSearch_ServiceAgent : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private Thread getClassRelationsAsync;
        private Thread getObjectRelationsAsync;

        private OntologyModDBConnector dbReaderOItem;
        private OntologyModDBConnector dbReaderClassAttributes;
        private OntologyModDBConnector dbReaderClassRelationsLeftRight;
        private OntologyModDBConnector dbReaderClassRelationsRightLeft;

        private OntologyModDBConnector dbReaderObjectClassses;
        private OntologyModDBConnector dbReaderObjectAttributes;
        private OntologyModDBConnector dbReaderObjectRelLeftRight;
        private OntologyModDBConnector dbReaderObjectRelRightLeft;

        private clsOntologyItem resultClassRelations;
        public clsOntologyItem ResultClassRelations
        {
            get
            {
                return resultClassRelations;
            }
            set
            {
                resultClassRelations = value;
                RaisePropertyChanged(Notifications.NotifyChanges.ElasticServiceAgent_ResultClassRelations);
            }

        }

        private clsOntologyItem resultObjectRelations;
        public clsOntologyItem ResultObjectRelations
        {
            get
            {
                return resultObjectRelations;
            }
            set
            {
                resultObjectRelations = value;
                RaisePropertyChanged(Notifications.NotifyChanges.ElasticServiceAgent_ResultObjectRelations);
            }

        }

        public clsOntologyItem RefClass { get; set; }
        public clsOntologyItem RefObject { get; set; }
        public List<clsClassAtt> ClassAttributes
        {
            get
            {
                return dbReaderClassAttributes.ClassAtts;
            }
        }
        public List<clsClassRel> ClassRelations
        {
            get
            {
                var classRelations = dbReaderClassRelationsLeftRight.ClassRels;
                classRelations.AddRange(dbReaderClassRelationsRightLeft.ClassRels);

                return classRelations;
            }
        }

        public List<clsOntologyItem> ObjectClasses
        {
            get
            {
                return dbReaderObjectClassses.Classes1;
            }
        }

        public List<clsObjectAtt> ObjectAttributes
        {
            get { return dbReaderObjectAttributes.ObjAtts; }
        }

        public List<clsObjectRel> ObjectRelations
        {
            get
            {
                var objectRelations = dbReaderObjectRelLeftRight.ObjectRels;
                objectRelations.AddRange(dbReaderObjectRelRightLeft.ObjectRels);

                return objectRelations;
            }
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            return dbReaderOItem.GetOItem(id, type);
        }
        public clsOntologyItem GetClassRelations(clsOntologyItem refClass)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            RefClass = refClass;

            if (RefClass == null)
            {
                ResultClassRelations = localConfig.Globals.LState_Nothing.Clone();
            }

            if (getClassRelationsAsync != null)
            {
                try
                {
                    getClassRelationsAsync.Abort();
                }
                catch(Exception ex)
                {

                }
                
            }

            getClassRelationsAsync = new Thread(GetClassRelationsAsync);
            getClassRelationsAsync.Start();

            return result;
        }

        public clsOntologyItem GetObjectRelations(clsOntologyItem refObject)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            RefObject = refObject;

            if (RefObject == null)
            {
                ResultObjectRelations = localConfig.Globals.LState_Nothing.Clone();
            }

            if (getObjectRelationsAsync != null)
            {
                try
                {
                    getObjectRelationsAsync.Abort();
                }
                catch (Exception ex)
                {

                }

            }

            getObjectRelationsAsync = new Thread(GetObjectRelationsAsync);
            getObjectRelationsAsync.Start();

            return result;
        }

        private void GetClassRelationsAsync()
        {
            var searchClassAttributes = new List<clsClassAtt> { new clsClassAtt
                {
                    ID_Class = RefClass.GUID
                }
            };

            var result = dbReaderClassAttributes.GetDataClassAtts(searchClassAttributes);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultClassRelations = result;
            
            var searchClassRelations = new List<clsClassRel> {  new clsClassRel
                    {
                        ID_Class_Left = RefClass.GUID
                    }
                };

            result = dbReaderClassRelationsLeftRight.GetDataClassRel(searchClassRelations);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultClassRelations = result;

            searchClassRelations = new List<clsClassRel> {  new clsClassRel
                    {
                        ID_Class_Right = RefClass.GUID
                    }
                };

            result = dbReaderClassRelationsRightLeft.GetDataClassRel(searchClassRelations);

            ResultClassRelations = result;
        }

        private void GetObjectRelationsAsync()
        {
            

            var searchObjectAttributes = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_Object = RefObject.GUID
                }
            };

            var result = dbReaderObjectAttributes.GetDataObjectAtt(searchObjectAttributes);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultObjectRelations = result;

            var searchObjectRelLeftRight = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = RefObject.GUID
                }
            };

            result = dbReaderObjectRelLeftRight.GetDataObjectRel(searchObjectRelLeftRight);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultObjectRelations = result;

            var searchObjectRelRightLeft = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = RefObject.GUID
                }
            };

            result = dbReaderObjectRelRightLeft.GetDataObjectRel(searchObjectRelRightLeft);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) ResultObjectRelations = result;

            var searchObjectClasses = new List<clsOntologyItem> {  new clsOntologyItem
                {
                    GUID = RefObject.GUID_Parent
                }
            };

            var objectClasses = dbReaderObjectRelLeftRight.ObjectRels.Where(relItem => relItem.Ontology == localConfig.Globals.Type_Object).GroupBy(objItem => new { IdClass = objItem.ID_Parent_Other }).ToList();
            
            var objectClasses1 = dbReaderObjectRelLeftRight.ObjectRels.Where(relItem => relItem.Ontology == localConfig.Globals.Type_Object).GroupBy(objItem => new { IdClass = objItem.ID_Parent_Object }).ToList();

            searchObjectClasses.AddRange(objectClasses.Select(objCls => new clsOntologyItem { GUID = objCls.Key.IdClass }));
            searchObjectClasses.AddRange(objectClasses1.Select(objCls => new clsOntologyItem { GUID = objCls.Key.IdClass }));

            result = dbReaderObjectClassses.GetDataClasses(searchObjectClasses);

            ResultObjectRelations = result;
        }

        public ElasticSearch_ServiceAgent(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReaderClassAttributes = new OntologyModDBConnector(localConfig.Globals);
            dbReaderClassRelationsLeftRight = new OntologyModDBConnector(localConfig.Globals);
            dbReaderClassRelationsRightLeft = new OntologyModDBConnector(localConfig.Globals);
            dbReaderOItem = new OntologyModDBConnector(localConfig.Globals);

            dbReaderObjectClassses = new OntologyModDBConnector(localConfig.Globals);
            dbReaderObjectAttributes = new OntologyModDBConnector(localConfig.Globals);
            dbReaderObjectRelLeftRight = new OntologyModDBConnector(localConfig.Globals);
            dbReaderObjectRelRightLeft = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
